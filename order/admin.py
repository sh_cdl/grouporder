from django.contrib import admin
from .models import People, Status, Restaurant, \
                    Dish, OrderList, Order, Timestamp

class PeopleAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'money_left')

class StatusAdmin(admin.ModelAdmin):
    list_display = ('usage', 'status')

class TimestampAdmin(admin.ModelAdmin):
    list_display = ('usage', 'timestamp')

class DishInline(admin.TabularInline):
    model = Dish
    extra = 0

class RestaurantAdmin(admin.ModelAdmin):
    fieldsets = [(None, {'fields': ['name', 'status']})]
    inlines = [DishInline]
    list_display = ('name', 'status')

class OrderInline(admin.TabularInline):
    model = Order
    extra = 0

class OrderListAdmin(admin.ModelAdmin):
    fieldsets = [(None, {'fields': ['restaurant', 'timestamp']})]
    inlines = [OrderInline]
    list_display = ('restaurant', 'timestamp')

# Register your models here.
admin.site.register(People, PeopleAdmin)
admin.site.register(Timestamp, TimestampAdmin)
admin.site.register(Status, StatusAdmin)
admin.site.register(Restaurant, RestaurantAdmin)
admin.site.register(OrderList, OrderListAdmin)