# coding=utf-8

from django.db import models
import os
import json

logo_path = 'logo/'
dish_path = 'dish/'
default_image = 'default.jpg'
max_length = 100

# Create your models here.
class People(models.Model):
    email = models.EmailField()
    name = models.CharField(max_length=max_length)
    money_left = models.FloatField()
    def __str__(self):
        return str(self.email)

class Timestamp(models.Model):
    usage = models.CharField(max_length=max_length)
    timestamp = models.DateTimeField()
    def __str__(self):
        return str(self.usage)

class Status(models.Model):
    usage = models.CharField(max_length=max_length)
    status = models.BooleanField()
    def __str__(self):
        return str(self.usage)

class Restaurant(models.Model):
    identifier = models.CharField(max_length=max_length)
    name = models.CharField(max_length=max_length)
    status = models.BooleanField()
    logo  = models.ImageField('Logo', upload_to=logo_path, default=default_image)
    arrival = models.IntegerField()
    sales = models.IntegerField()
    deliver_cost = models.FloatField()
    minimum_expense = models.FloatField()
    rate = models.FloatField()
    name_for_url = models.CharField(max_length=max_length)
    def __str__(self):
        return self.name

class Dish(models.Model):
    restaurant = models.ForeignKey(Restaurant)
    group = models.CharField(max_length=max_length)
    rating = models.FloatField()
    tips = models.TextField()
    month_sales = models.IntegerField()
    rating_count = models.IntegerField()
    food_id = models.CharField(max_length=max_length)
    name = models.CharField(max_length=max_length)
    price = models.FloatField()
    image  = models.ImageField('Menu', upload_to=dish_path, default=default_image)
    def __str__(self):
        return '{0} {1}'.format(self.name, str(self.price))

class OrderList(models.Model):
    restaurant = models.ForeignKey(Restaurant)
    timestamp = models.DateTimeField()
    is_submitted = models.BooleanField()
    def __str__(self):
        return '{0} {1}'.format(self.restaurant.name, str(self.timestamp))

class Order(models.Model):
    order_list = models.ForeignKey(OrderList)
    customer = models.ForeignKey(People)
    dishes = models.ManyToManyField(Dish)
    #def __setattr__(self, key, value):
        #super(Order, self)._setattr__(key, value)
        #self.__dict__[key] = value
        #if key in ['order_list']:
            #self.dishes = Dish.objects.filter(restaurant = self.order_list.restaurant)

    def __str__(self):
        return self.customer.name
