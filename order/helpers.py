from .models import *
from .forms import OrderForm
from django.utils import timezone
from django.contrib.auth import authenticate, login, logout
from .adapter import *
from spider.spider.eleme_nearby_restaunts import *
from GR.settings import MEDIA_ROOT
from collections import OrderedDict
import shutil
import os

refresh_timestamp_usage = "refresh data"
#in minute
refresh_interval = 60*60*48
media_logo_path = MEDIA_ROOT + '/' + logo_path
media_dish_path = MEDIA_ROOT + '/' + dish_path

def get_status(usage):
    try:
        status = Status.objects.get(usage = usage)
    except Status.DoesNotExist:
        status = None
    if status != None:
        return status.status
    else:
        return False

def set_status(usage, status_flag):
    try:
        status = Status.objects.get(usage = usage)
    except Status.DoesNotExist:
        status = None
    if status != None:
        status.status = status_flag
        status.save()
        return status
    else:
        new_status = Status()
        new_status.usage = usage
        new_status.status = status_flag
        new_status.save()
        return new_status

def setup_order_list(restaurant):
    order_list = OrderList()
    order_list.timestamp = timezone.now()
    order_list.restaurant = restaurant
    order_list.is_submitted = False
    order_list.save()
    return  order_list

def update_customer_order(order_list, request):
        customer = People.objects.get(pk = request.POST['customer'])
        new_order_list = OrderList.objects.get(pk = request.POST['order_list'])
        if order_list != new_order_list:
            return
        try:
            order = order_list.order_set.get(customer = customer)
        except Order.DoesNotExist:
            order = None
        if order != None:
            for dish in order.dishes.all():
                order.dishes.remove(dish)

            for pk in dict(request.POST)['dishes']:
                order.dishes.add(Dish.objects.get(pk = pk))
            order.save()
            return order
        else:
            new_order = Order()
            new_order.order_list = order_list
            new_order.customer = customer
            new_order.save()
            for pk in dict(request.POST)['dishes']:
                new_order.dishes.add(Dish.objects.get(pk = pk))
                new_order.save()
            return new_order

def get_latest_order_list():
    order_lists = OrderList.objects.order_by('-timestamp')
    if order_lists.count() > 0:
        order_list = order_lists[0]
        return order_list
    return None

def deduct_expense_from_people(order_list):
    orders = order_list.order_set.all()
    for order in orders:
        for dish in order.dishes.all():
            order.customer.money_left -= dish.price
            order.customer.save()

neay_restuants_manager = ElemeNearbyRestauntManager()
def force_refresh_all():
    prepare_to_refresh()
    for restaurant in neay_restuants_manager.restuants(''):
        db_restaurant = save_ele_restaurant(restaurant)
        refresh_dish_in_restaurant(db_restaurant)

def refresh_nearby_restaurants():
    for restaurant in neay_restuants_manager.restuants(''):
        save_ele_restaurant(restaurant)

def refresh_dish_in_restaurant(restaurant):
    if restaurant.dish_set.all().count() > 0:
        return
    restaurant_object = neay_restuants_manager.restaunt_object(restaurant.identifier)
    menus = restaurant_object.load_menu()
    save_ele_menu(menus)

def stamp_now():
    try:
        refresh_timestamp = Timestamp.objects.all().get(usage = refresh_timestamp_usage)
    except Timestamp.DoesNotExist:
        refresh_timestamp = Timestamp()

    refresh_timestamp.usage = refresh_timestamp_usage
    refresh_timestamp.timestamp = timezone.now()
    refresh_timestamp.save()

def prepare_to_refresh():
    stamp_now()
    if Restaurant.objects.all().count() > 0:
        Restaurant.objects.all().delete()
    if Dish.objects.all().count() > 0:
        Dish.objects.all().delete()
    if Status.objects.all().count() > 0:
        Status.objects.all().delete()

    #print(media_logo_path)
    if os.path.exists(media_logo_path):
        shutil.rmtree(media_logo_path)
    if os.path.exists(media_dish_path):
        shutil.rmtree(media_dish_path)
    if not os.path.exists(MEDIA_ROOT):
        os.mkdir(MEDIA_ROOT)
    os.mkdir(media_logo_path)
    os.mkdir(media_dish_path)

def should_refresh_data():
    if Restaurant.objects.all().count() == 0 or not os.path.exists(media_logo_path):
        prepare_to_refresh()
        return True

    try:
        refresh_timestamp = Timestamp.objects.all().get(usage = refresh_timestamp_usage)
    except Timestamp.DoesNotExist:
        prepare_to_refresh()
        return  True

    now_timestamp = timezone.now()
    if (now_timestamp - refresh_timestamp.timestamp).total_seconds() > refresh_interval:
        prepare_to_refresh()
        return True
    else:
        return False

def build_dishes_in_group(restaurant):
    groups = OrderedDict()
    for dish in restaurant.dish_set.all():
        group_name = dish.group
        if not group_name in groups.keys():
            groups.__setitem__(group_name, [dish])
        else:
            groups[group_name].append(dish)

    return groups

def system_login(request):
    username = request.POST['Username']
    pw = request.POST['Password']
    user = authenticate(username=username, password=pw)
    if user is not None:
        if user.is_active:
            login(request, user)
            return True

    return False

def system_logout(request):
    logout(request)
