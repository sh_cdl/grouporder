# coding=utf-8

from django import forms
from django.utils import timezone
from .models import Order, OrderList, Restaurant

class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ('order_list', 'customer')

    dishes = forms.ModelMultipleChoiceField(queryset = None, widget=forms.CheckboxSelectMultiple())

    def __init__(self, order_list, customer, *args, **kwargs):
        initial = {
           'order_list': order_list,
           'customer': customer,
          }
        if 'initial' in kwargs.keys():
            kwargs['initial'].update(initial)
        else:
            kwargs['initial'] = initial
        # Initializing form only after you have set initial dict
        super(forms.ModelForm, self).__init__(*args, **kwargs)

        self.fields['dishes'].queryset = order_list.restaurant.dish_set.all()
        self.fields['order_list'].widget = forms.HiddenInput()
        self.fields['customer'].widget = forms.HiddenInput()
