from django.conf.urls import url
from . import views
from django.conf.urls.static import static
from django.conf import settings

# just for test
urlpatterns = [
    url(r'^login/$', views.login, name='login'),
    url(r'^inprocess/(?P<cookie>(.*))/$', views.in_process, name='in_process'),
    url(r'^restaurants/$', views.restaurant_list, name='restaurant_list'),
    url(r'^restaurants/(?P<restaurant_pk>[0-9]+)/$', views.group_menu, name = 'group_menu'),
    url(r'^restaurants/(?P<restaurant_pk>[0-9]+)/startorder/$', views.start_order, name = 'start_order'),
    url(r'^restaurants/(?P<restaurant_pk>[0-9]+)/(?P<order_pk>[0-9]+)/$', views.in_order, name = 'in_order'),
    url(r'^restaurants/(?P<restaurant_pk>[0-9]+)/(?P<order_pk>[0-9]+)/(?P<user>[A-Z,a-z,0-9]+)/$', views.individual_menu, name = 'individual_menu'),
    url(r'^endorder/(?P<order_pk>[0-9]+)/$', views.end_order, name = 'end_order'),
    url(r'^orderlist/$', views.order_list, name='order_list'),
    url(r'^error/(?P<info>(.*))/$', views.error, name = 'error'),
    url(r'^logout/$', views.logout, name = 'logout')
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
