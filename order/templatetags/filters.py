from django.template.defaulttags import register
from django.utils.safestring import mark_safe
from django.core import serializers
import json
from django.core.serializers.json import DjangoJSONEncoder

@register.filter(name = 'find_dishes_by_group')
def find_dishes_by_group(dic, arg):
    return dic.get(arg)

@register.filter(name = 'find_dish_by_pk')
def find_dish_by_pk(dishes, arg):
    dish = dishes.get(pk = arg)
    return dish

@register.filter(name = 'get_dish_price_by_pk')
def get_dish_price_by_pk(dishes, arg):
    dish = dishes.get(pk = arg)
    return dish.price

@register.filter(name = 'total_consumption_per_order')
def total_consumption_per_order(dishes):
    consumption = 0
    for dish in dishes:
        consumption += dish.price

    return consumption

@register.filter(name = 'total_consumption_per_order_list')
def total_consumption_per_order_list(order_list):
    consumption = 0
    for order in order_list.order_set.all():
            for dish in order.dishes.all():
                    consumption += dish.price
    return consumption

@register.filter(name='js')
def js(obj):
    return json.dumps(obj.__dict__, cls=DjangoJSONEncoder)