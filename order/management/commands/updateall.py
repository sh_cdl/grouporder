from django.core.management.base import BaseCommand, CommandError
from order.helpers import neay_restuants_manager, prepare_to_refresh, \
    refresh_nearby_restaurants, refresh_dish_in_restaurant
from order.adapter import save_ele_restaurant

class Command(BaseCommand):
    help = 'Pull new data from 3rd-party server'

    def handle(self, *args, **options):
        prepare_to_refresh()
        for restaurant in neay_restuants_manager.restuants(''):
            db_restaurant = save_ele_restaurant(restaurant)
            refresh_dish_in_restaurant(db_restaurant)

            self.stdout.write('pull restaurant "%s"' % db_restaurant.name)