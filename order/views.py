# coding=utf-8

from django.shortcuts import render, get_object_or_404, redirect
from .models import *
from .forms import OrderForm
from .mail import send_order_mail
from .helpers import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from spider.spider.login import *

is_in_ordering_usage = "is_in_ordering"
is_email_sent_usage = "is_email_sent"

# Create your views here.
def login(request):
    cookie = get_captcha()
    return render(request, 'order/login.html', {'cookie' : cookie})

def logout(request):
    system_logout(request)
    return redirect('login')

def in_process(request, cookie):
    username = request.POST['Username']
    pw = request.POST['Password']
    captcha = request.POST['Captcha']
    login_info = web_login(username, pw, captcha, cookie)
    if login_info['success']:
        if system_login(request):
            return redirect('restaurant_list')
    return redirect('error', 'Failed to login')

def restaurant_list(request):
    if not request.user.is_authenticated():
        return redirect('error', 'Not authenticated')

    lastest_order = get_latest_order_list()

    #pull data everyday on 9:00
    #if should_refresh_data():
        #refresh_nearby_restaurants()

    paginator = Paginator(Restaurant.objects.all(), 10) # Show 5 restaurants per page
    page = request.GET.get('page')
    try:
        restaurants = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        restaurants = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        restaurants = paginator.page(paginator.num_pages)
    return  render(request, 'order/restaurant_list.html', {'restaurants' : restaurants, 'lastest_order':lastest_order})

def order_list(request):
    lastest_order = get_latest_order_list()
    order_lists = OrderList.objects.all().order_by('-timestamp')
    return render(request, 'order/order_list.html', {'lastest_order':lastest_order, 'order_lists' : order_lists})

def group_menu(request, restaurant_pk):
    if not request.user.is_authenticated():
        return redirect('error', 'Not authenticated')

    is_in_ordering = get_status(is_in_ordering_usage)
    restaurant = Restaurant.objects.get(pk = restaurant_pk)
    if is_in_ordering:
        order_list = get_latest_order_list()
        if order_list != None:
            return redirect('in_order', restaurant_pk=order_list.restaurant.pk, order_pk=order_list.pk)
        else:
            return redirect('error', 'Wrong status')
    else:
        refresh_dish_in_restaurant(restaurant)
        dishes = build_dishes_in_group(restaurant)
        return render(request, 'order/group_menu.html', {'dishes':dishes, 'restaurant':restaurant})

def start_order(request, restaurant_pk):
    if not request.user.is_authenticated():
        return redirect('error', 'Not authenticated')

    restaurant = Restaurant.objects.get(pk = restaurant_pk)
    order_list = setup_order_list(restaurant)
    set_status(is_in_ordering_usage, True)
    #redirect to in_order page
    return redirect('in_order', restaurant_pk=restaurant_pk, order_pk=order_list.pk)

def individual_menu(request, restaurant_pk, order_pk, user):
    order_list = OrderList.objects.get(pk = order_pk)
    customer = People.objects.get(name=user)
    form = OrderForm(order_list, customer)
    dishes = build_dishes_in_group(order_list.restaurant)
    try:
        order = order_list.order_set.get(customer=customer)
    except Order.DoesNotExist:
        order = None
    selected_dishes = []
    if order != None:
        selected_dishes += order.dishes.all()
    return render(request, 'order/individual_menu.html', {'customer' : customer, 'form' : form, 'order_list' : order_list, 'dishes' : dishes, 'selected_dishes':selected_dishes})

def in_order(request, restaurant_pk, order_pk):
    order_list = OrderList.objects.get(pk = order_pk)
    latest_order_list = get_latest_order_list()
    is_ordering = get_status(is_in_ordering_usage)
    is_current_order_list = (order_list == latest_order_list)
    #do nothing if the order list is already out-of-date
    if is_current_order_list:
        if request.method == 'POST' and not order_list.is_submitted:
            update_customer_order(order_list, request)
        if not get_status(is_email_sent_usage) and not order_list.is_submitted:
            send_order_mail(request.build_absolute_uri())
            set_status(is_email_sent_usage, True)
    return render(request, 'order/in_order.html', {'order_list' : order_list, 'is_ordering' : is_ordering, 'is_current_order_list' : is_current_order_list})

def end_order(request, order_pk):
    if not request.user.is_authenticated():
        return redirect('error', 'Not authenticated')

    set_status(is_in_ordering_usage, False)
    set_status(is_email_sent_usage, False)
    order_list = OrderList.objects.get(pk = order_pk)
    order_list.is_submitted = True
    order_list.save()
    deduct_expense_from_people(order_list)
    return render(request, 'order/end_order.html', {'order_list' : order_list})

def error(request, info):
    set_status(is_in_ordering_usage, False)
    set_status(is_email_sent_usage, False)
    return render(request, 'order/error.html', {'info' : info})