from .models import *
import urllib
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
import os

def save_ele_menu(menus):
    for menu in menus:
        group = menu['name']
        for food in menu['foods']:
            db_dish = Dish()
            db_dish.group = group
            restaurant_id = food['restaurant_id']
            db_dish.restaurant = Restaurant.objects.all().get(identifier=restaurant_id)
            for specfood in food['specfoods']:
                db_dish.food_id = specfood['food_id']
                db_dish.name = specfood['name']
                db_dish.price = specfood['price']
            db_dish.rating = food['rating']
            db_dish.tips = food['tips']
            db_dish.month_sales = food['month_sales']
            db_dish.rating_count = food['rating_count']

            web_image_path = food['image_path']
            image_name = db_dish.restaurant.name+'-'+db_dish.name
            local_image_path = "dish/" + image_name
            if not web_image_path=='':
                if not os.path.exists(local_image_path):
                    image_temp = NamedTemporaryFile(delete=True)
                    image_temp.write(urllib.request.urlopen(web_image_path).read())
                    image_temp.flush()
                    db_dish.image.save(image_name, File(image_temp), save=False)
                else:
                    db_dish.image = local_image_path

            db_dish.save()
    return db_dish

def save_ele_restaurant(ele_restaurant):
    db_restaurant = Restaurant()
    db_restaurant.status = True
    db_restaurant.name = ele_restaurant['name']
    db_restaurant.arrival = ele_restaurant['order_lead_time']
    db_restaurant.deliver_cost = ele_restaurant['delivery_fee']
    db_restaurant.minimum_expense = ele_restaurant['minimum_order_amount']
    db_restaurant.sales = ele_restaurant['month_sales']
    db_restaurant.rate = ele_restaurant['rating']
    db_restaurant.identifier = ele_restaurant['id']
    db_restaurant.name_for_url = ele_restaurant['name_for_url']

    web_image_path = ele_restaurant['image_path']
    image_name = db_restaurant.name_for_url
    local_image_path = 'logo/' + image_name
    if not web_image_path=='':
        if not os.path.exists(local_image_path):
            image_temp = NamedTemporaryFile(delete=True)
            image_temp.write(urllib.request.urlopen(web_image_path).read())
            image_temp.flush()
            db_restaurant.logo.save(db_restaurant.name_for_url, File(image_temp), save=False)
        else:
            db_restaurant.logo = local_image_path

    db_restaurant.save()
    return db_restaurant